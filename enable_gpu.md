# Run Tensorflow with GTX1050 in Ubuntu16.04

This article explains how to get run Tensorflow in Nvidia GeForce GTX1050;

This article includes two parts: 

1. the dependency, explains the relationship between the components and their purpose;
2. steps to run tensorflow with GTX1050 in ubuntu16.04
3. run docker

the first part explains how to enable the GTX1050 in ubuntu16.04 to run tensorflow; the second part explains how to run them with docker.


## following: 
1. local run, with tensorflow-gpu-1.3, checked,
2. local run, with tensorflow-gpu-1.6+, unchecked
3. docker run, ubuntu base, unchecked,
4. docker run, nvidia-docker image base, unchecked, 
5. nividia-docker run, 

## Run Tensorflow-gpu-1.3
### Install driver
using the package manager installation)
```
sudo apt-get install nvidia-375
```

upon complete, reboot your system and verify with below command
```
nvidia-smi -a 
```

The result looks like this: 
[smi-ouput.png](./images/smi-ouput.png)

### Install cuda

1. download CUDA from this [CUDA archive site](https://developer.nvidia.com/cuda-80-ga2-download-archive):  

select the the package:

![cuda-selected.png](./images/cuda-selected.png) 

2. goes to the download folder, run the following command: 

```
sudo dpkg -i cuda-repo-ubuntu1604-8-0-local-ga2_8.0.61-1_amd64.deb
sudo apt-get update
sudo apt-get install cuda
```

The CUDA is installed in folder: `/usr/local/`, the structure may look like this:

```
➜  local pwd                     
/usr/local

➜  local tree -L 1
.
├── bin
├── cuda -> cuda-8.0
├── cuda-8.0
└── ....

```

Now, export variables:

```
export PATH=/usr/local/cuda/bin:$PATH
export LD_LIBRARY_PATH=/usr/local/cuda/lib64:$LD_LIBRARY_PATH
```

export LD_LIBRARY_PATH=/usr/local/cuda/targets/x86_64-linux/lib/stubs/:$LD_LIBRARY_PATH


3. verify the installation
to verify if installed successfully, we need to compile some files to test;

```
cd /usr/local/cuda/samples
sudo make
```

wait till the compilation done, then navigate to the folder: 
`/usr/local/cuda/samples/1_Utilities/deviceQuery`, and run command 

`./deviceQuery`, if you could see below output, then your CUDA installed successfully;

![cuda-verify-out.png](./images/cuda-verify-out.png)


to check the cuda version:
```➜  local nvcc --version
nvcc: NVIDIA (R) Cuda compiler driver
Copyright (c) 2005-2017 NVIDIA Corporation
Built on Fri_Sep__1_21:08:03_CDT_2017
Cuda compilation tools, release 9.0, V9.0.176
```


### Install cudnn

1. download the cudnn from this [cudnn archive site](https://developer.nvidia.com/rdp/cudnn-archive)

select the the package:

![cudnn-selected.png](./images/cudnn-selected.png) 

2. goes to the download folder, run the following command:

```
tar -xzvf cudnn-8.0-linux-x64-v6.0.tgz
sudo cp cuda/lib64/* /usr/local/cuda/lib64/
sudo cp cuda/include/cudnn.h /usr/local/cuda/include/

```

to check the cudnn version:

```
➜  local cat /usr/local/cuda/include/cudnn.h | grep CUDNN_MAJOR -A 2
#define CUDNN_MAJOR 7
#define CUDNN_MINOR 0
#define CUDNN_PATCHLEVEL 5
--
#define CUDNN_VERSION    (CUDNN_MAJOR * 1000 + CUDNN_MINOR * 100 + CUDNN_PATCHLEVEL)

#include "driver_types.h"
```

### Local Machine 

#### Run tensorflow
create a conda environment, [python=2.7]: 

```
conda create -n tensorflow-gpu pip python=2.7

source activate tensorflow-gpu
pip install tensorflow-gpu==1.3.0  

```

python run the following code: 
```
touch test_tf_gpu.py
cat <<EOT>> test_tf_gpu.py
import tensorflow as tf
hello = tf.constant('Hello, Tensorflow!')
sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
print(sess.run(hello))
EOT
python test_tf_gpu.py
```

if you cloud see the following output, then the GPU is successfully enabled; 

```
(tensorflow-gpu) ➜  mnist_with_tensorflow git:(master) ✗ python test_tf_gpu.py   
2018-05-08 15:17:45.147618: I tensorflow/core/platform/cpu_feature_guard.cc:140] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
2018-05-08 15:17:45.246940: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:898] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2018-05-08 15:17:45.247250: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1356] Found device 0 with properties: 
name: GeForce GTX 1050 major: 6 minor: 1 memoryClockRate(GHz): 1.493
pciBusID: 0000:01:00.0
totalMemory: 1.95GiB freeMemory: 1.03GiB
2018-05-08 15:17:45.247265: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1435] Adding visible gpu devices: 0
2018-05-08 15:17:45.429802: I tensorflow/core/common_runtime/gpu/gpu_device.cc:923] Device interconnect StreamExecutor with strength 1 edge matrix:
2018-05-08 15:17:45.429829: I tensorflow/core/common_runtime/gpu/gpu_device.cc:929]      0 
2018-05-08 15:17:45.429834: I tensorflow/core/common_runtime/gpu/gpu_device.cc:942] 0:   N 
2018-05-08 15:17:45.429936: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1053] Created TensorFlow device (/job:localhost/replica:0/task:0/device:GPU:0 with 776 MB memory) -> physical GPU (device: 0, name: GeForce GTX 1050, pci bus id: 0000:01:00.0, compute capability: 6.1)
Device mapping:
/job:localhost/replica:0/task:0/device:GPU:0 -> device: 0, name: GeForce GTX 1050, pci bus id: 0000:01:00.0, compute capability: 6.1
2018-05-08 15:17:45.437861: I tensorflow/core/common_runtime/direct_session.cc:284] Device mapping:
/job:localhost/replica:0/task:0/device:GPU:0 -> device: 0, name: GeForce GTX 1050, pci bus id: 0000:01:00.0, compute capability: 6.1

Const: (Const): /job:localhost/replica:0/task:0/device:CPU:0
2018-05-08 15:17:45.438300: I tensorflow/core/common_runtime/placer.cc:886] Const: (Const)/job:localhost/replica:0/task:0/device:CPU:0
Hello, Tensorflow!

```


#### Run Tensorflow-gpu-1.8
turns out that the 1.8 works correctly here;
the cuda and cudnn version is 9.0 and 7.0, what if i change back to cuda8.0

link the cuda 
```
sudo ln -sf /usr/local/cuda-8.0 /usr/local/cuda
```

```➜  local printenv | grep LD_LIBRARY_PATH
LD_LIBRARY_PATH=/usr/local/cuda/lib64:/usr/local/cuda/lib64:

➜  local nvcc --version
nvcc: NVIDIA (R) Cuda compiler driver
Copyright (c) 2005-2016 NVIDIA Corporation
Built on Tue_Jan_10_13:22:03_CST_2017
Cuda compilation tools, release 8.0, V8.0.61

➜  local cat /usr/local/cuda/include/cudnn.h | grep CUDNN_MAJOR -A 2
#define CUDNN_MAJOR      6
#define CUDNN_MINOR      0
#define CUDNN_PATCHLEVEL 21
--
#define CUDNN_VERSION    (CUDNN_MAJOR * 1000 + CUDNN_MINOR * 100 + CUDNN_PATCHLEVEL)

#include "driver_types.h"
```

still works!!!

**that means, tensorflow-gpu-1.8 also works with cuda8.0 and cudnn6.0**
see [here](https://github.com/tensorflow/tensorflow/releases):

```
Deprecations
TensorFlow 1.7 may be the last time we support Cuda versions below 8.0.
Starting with TensorFlow 1.8 release, 8.0 will be the minimum supported
version.
TensorFlow 1.7 may be the last time we support cuDNN versions below 6.0.
Starting with TensorFlow 1.8 release, 6.0 will be the minimum supported
version. 
```



### Run in docker

#### build from scrach
```
docker run -it --privileged --device /dev/nvidia0:/dev/nvidia0 --device /dev/nvidiactl:/dev/nvidiactl --device /dev/nvidia-modeset:/dev/nvidia-modeset --device /dev/nvidia-uvm:/dev/nvidia-uvm --device /dev/nvidia-uvm-tools:/dev/nvidia-uvm-tools --mount type=bind,src=/home/zhangxg/work/gitrepo/github,dst=/host cr.d.xiaomi.net/zhangxiaogang/tensorflow-gpu-nvidia:1.8 bash
```

#### build with nvidia-docker

```
nvidia-docker run -it --privileged --mount type=bind,src=/home/zhangxg/work/gitrepo/github,dst=/host cr.d.xiaomi.net/zhangxiaogang/tensorflow-gpu-nvidia:1.8 bash 
```

To run in the docker requires two steps: 
first, we need to make a docker image, with the driver, cuda and cudnn all installed just the same as what we have done in host ubuntu16.04;
second, device mapped in when running the docker container; 

### build the docker image
attachment 2 gives a full list of the Dockerfile;

build with the following command, let's say your docker image is named as `ubuntu16.04-tensorflow1.3-gpu`

```
sudo docker build -t ubuntu16.04-tensorflow1.3-gpu .
```

wait till it'd one;

now check if the images built successfully:

```
sudo docker images
```

### run the docker

```
sudo docker run -ti 
```


## Resources

1. [nvidia official doc for installation](http://docs.nvidia.com/cuda/1.cuda-installation-guide-linux/#axzz4VZnqTJ2A)
1. [nvidia drivers](http://www.nvidia.com/Download/index.aspx)
1. [CUDA Toolkit 9.1](https://developer.nvidia.com/1.cuda-downloads?target_os=Linux)
1. [CUDA Toolkit legacy](https://developer.nvidia.com/cuda-toolkit-archive)
1. [cudann download page](https://developer.nvidia.com/rdp/cudnn-download)
1. [cudnn archive](https://developer.nvidia.com/rdp/cudnn-archive)
1. [ubuntu docker hub](https://hub.docker.com/r/library/ubuntu/)

docker run, with tf1.8

```root@e2c83a4ac96a:/# python test_tf_gpu.py 
Traceback (most recent call last):
  File "/usr/local/lib/python3.5/dist-packages/tensorflow/python/pywrap_tensorflow.py", line 58, in <module>
    from tensorflow.python.pywrap_tensorflow_internal import *
  File "/usr/local/lib/python3.5/dist-packages/tensorflow/python/pywrap_tensorflow_internal.py", line 28, in <module>
    _pywrap_tensorflow_internal = swig_import_helper()
  File "/usr/local/lib/python3.5/dist-packages/tensorflow/python/pywrap_tensorflow_internal.py", line 24, in swig_import_helper
    _mod = imp.load_module('_pywrap_tensorflow_internal', fp, pathname, description)
  File "/usr/lib/python3.5/imp.py", line 242, in load_module
    return load_dynamic(name, filename, file)
  File "/usr/lib/python3.5/imp.py", line 342, in load_dynamic
    return _load(spec)
ImportError: libcublas.so.9.0: cannot open shared object file: No such file or directory

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "test_tf_gpu.py", line 1, in <module>
    import tensorflow as tf
  File "/usr/local/lib/python3.5/dist-packages/tensorflow/__init__.py", line 24, in <module>
    from tensorflow.python import pywrap_tensorflow  # pylint: disable=unused-import
  File "/usr/local/lib/python3.5/dist-packages/tensorflow/python/__init__.py", line 49, in <module>
    from tensorflow.python import pywrap_tensorflow
  File "/usr/local/lib/python3.5/dist-packages/tensorflow/python/pywrap_tensorflow.py", line 74, in <module>
    raise ImportError(msg)
ImportError: Traceback (most recent call last):
  File "/usr/local/lib/python3.5/dist-packages/tensorflow/python/pywrap_tensorflow.py", line 58, in <module>
    from tensorflow.python.pywrap_tensorflow_internal import *
  File "/usr/local/lib/python3.5/dist-packages/tensorflow/python/pywrap_tensorflow_internal.py", line 28, in <module>
    _pywrap_tensorflow_internal = swig_import_helper()
  File "/usr/local/lib/python3.5/dist-packages/tensorflow/python/pywrap_tensorflow_internal.py", line 24, in swig_import_helper
    _mod = imp.load_module('_pywrap_tensorflow_internal', fp, pathname, description)
  File "/usr/lib/python3.5/imp.py", line 242, in load_module
    return load_dynamic(name, filename, file)
  File "/usr/lib/python3.5/imp.py", line 342, in load_dynamic
    return _load(spec)
ImportError: libcublas.so.9.0: cannot open shared object file: No such file or directory

```

<!--
# we know tensorflow depends on the cuda and cudnn, 
cuda and cudnn depends on the driver, 

we have two constraints, the gpu card type determines which cuda and cudnn we can use, in turn they two determines which tensorflow version we can use, 

it also has dependency on the os, and kernel. 

first question: where to get the tensorflow and cuX's dependency?  todo: 

## todos: 
1. enable gpu in docker container; how? 

```
root@3c042f56f945:/host/learning_deeper/mnist_with_tensorflow# python mnist_training.py 
Extracting MNIST-data/train-images-idx3-ubyte.gz
Extracting MNIST-data/train-labels-idx1-ubyte.gz
Extracting MNIST-data/t10k-images-idx3-ubyte.gz
Extracting MNIST-data/t10k-labels-idx1-ubyte.gz
INFO:tensorflow:Using default config.
INFO:tensorflow:Using config: {'_save_checkpoints_secs': 600, '_session_config': None, '_keep_checkpoint_max': 5, '_tf_random_seed': 1, '_keep_checkpoint_every_n_hours': 10000, '_log_s
tep_count_steps': 100, '_save_checkpoints_steps': None, '_model_dir': '/tmp/mnist_convnet_model', '_save_summary_steps': 100}
INFO:tensorflow:Create CheckpointSaverHook.
2018-03-28 02:00:37.246618: E tensorflow/stream_executor/cuda/cuda_driver.cc:406] failed call to cuInit: CUresult(-1)
2018-03-28 02:00:37.246656: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:158] retrieving CUDA diagnostic information for host: 3c042f56f945
2018-03-28 02:00:37.246664: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:165] hostname: 3c042f56f945
2018-03-28 02:00:37.246750: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:189] libcuda reported version is: Not found: was unable to find libcuda.so DSO loaded into this progra
m
2018-03-28 02:00:37.246775: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:369] driver version file contents: """NVRM version: NVIDIA UNIX x86_64 Kernel Module  384.111  Tue Dec
 19 23:51:45 PST 2017
GCC version:  gcc version 5.4.0 20160609 (Ubuntu 5.4.0-6ubuntu1~16.04.9) 
"""
2018-03-28 02:00:37.246791: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:193] kernel reported version is: 384.111.0
INFO:tensorflow:Saving checkpoints for 1 into /tmp/mnist_convnet_model/model.ckpt.
INFO:tensorflow:probabilities = [[ 0.0917701   0.12164007  0.07844587  0.10742215  0.10315583  0.09044013
   0.09593881  0.11132244  0.09557512  0.10428946]

```

```
root@3c042f56f945:/host/learning_deeper/mnist_with_tensorflow# printenv | grep LD
LD_LIBRARY_PATH=/usr/lib64/nvidia/:/usr/lib64/nvidia/:/usr/local/cuda/extras/CUPTI/lib64:/hadoop/hadoop-2.4.0-mdh2.3.31/lib:/hadoop/jdk1.6.0_37/jre/lib/amd64/server/:/hadoop/hadoop-2.4.0-mdh2.3.31/lib/native/:/hadoop/jdk1.6.0_37//jre/lib/amd64/server:/usr/local/cuda/lib64/stubs/
OLDPWD=/host/learning_deeper

```
```
2018-03-28 02:12:57.602097: E tensorflow/stream_executor/cuda/cuda_driver.cc:406] failed call to cuInit: CUresult(-1)
2018-03-28 02:12:57.602131: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:158] retrieving CUDA diagnostic information for host: 3c042f56f945
2018-03-28 02:12:57.602140: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:165] hostname: 3c042f56f945
2018-03-28 02:12:57.602287: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:189] libcuda reported version is: Not found: was unable to find libcuda.so DSO loaded into this progra
m
2018-03-28 02:12:57.602333: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:369] driver version file contents: """NVRM version: NVIDIA UNIX x86_64 Kernel Module  384.111  Tue Dec
 19 23:51:45 PST 2017
GCC version:  gcc version 5.4.0 20160609 (Ubuntu 5.4.0-6ubuntu1~16.04.9) 
"""
```

driver host:
```
(tensorflow-gpu) ➜  ~ apt-cache search nvidia | grep -P '^nvidia-[0-9]+\s'
nvidia-331 - Transitional package for nvidia-331
nvidia-346 - Transitional package for nvidia-346
nvidia-304 - NVIDIA legacy binary driver - version 304.135
nvidia-340 - NVIDIA binary driver - version 340.104
nvidia-361 - Transitional package for nvidia-367
nvidia-367 - Transitional package for nvidia-375
nvidia-375 - Transitional package for nvidia-384
nvidia-384 - NVIDIA binary driver - version 384.111
nvidia-352 - Transitional package for nvidia-375

```

driver docker:
```
root@79035f1d792d:/usr/local/cuda/lib64/stubs# apt-cache search nvidia | grep -P '^nvidia-[0-9]+\s'
nvidia-331 - Transitional package for nvidia-331
nvidia-346 - Transitional package for nvidia-346
nvidia-304 - NVIDIA legacy binary driver - version 304.135
nvidia-340 - NVIDIA binary driver - version 340.102
nvidia-361 - Transitional package for nvidia-367
nvidia-375 - Transitional package for nvidia-384
nvidia-384 - NVIDIA binary driver - version 384.90
nvidia-352 - Transitional package for nvidia-375
nvidia-367 - Transitional package for nvidia-384

```

## problems: 
1. dependency nvidia-384 not found: 
== sudo apt-get install nvidia-384

2. disable cuda9 ppa; 

cloudml uses `nvidia-cuda:8.0-cudnn5-devel-ubuntu16.04`
[docker file](https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/8.0/devel/cudnn5/Dockerfile)

links: 

https://medium.com/@acrosson/installing-nvidia-cuda-cudnn-tensorflow-and-keras-69bbf33dce8a

https://github.com/nicolaifsf/Installing-Tensorflow-with-GPU [***


2018-04-03 07:01:21.384037: E tensorflow/stream_executor/cuda/cuda_dnn.cc:378] Loaded runtime CuDNN library: 7102 (compatibility version 7100) but source was compiled with 7004 (compatibility version 7000).  If using a binary install, upgrade your cudaDNN library to match.  If building from sources, make sure the library loaded at runtime matches a compatible version specified during compile configuration.



-->

cp -P cuda/include/cudnn.h /usr/local/cuda/include
cp -P cuda/lib64/libcudnn* /usr/local/nvidia/lib64




docker run -it -p 127.0.0.1:8080:6006 --mount type=bind,src=/home/zhangxg/work/gitrepo/github,dst=/host -v /usr/lib/x86_64-linux-gnu/libcuda.so:/usr/lib/x86_64-linux-gnu/libcuda.so -v /usr/lib/x86_64-linux-gnu/libcuda.so.1:/usr/lib/x86_64-linux-gnu/libcuda.so.1 -v /usr/lib/x86_64-linux-gnu/libcuda.so.384.111:/usr/lib/x86_64-linux-gnu/libcuda.so.384.111 --device /dev/nvidia0:/dev/nvidia0 --device /dev/nvidiactl:/dev/nvidiactl --device /dev/nvidia-modeset:/dev/nvidia-modeset --device /dev/nvidia-uvm:/dev/nvidia-uvm --device /dev/nvidia-uvm-tools:/dev/nvidia-uvm-tools cr.d.xiaomi.net/cloud-ml/tensorflow-gpu:1.6.0-xm1.0.0-cuda9 bash


nvidia-docker run -it --privileged -p 127.0.0.1:8080:6006 --mount type=bind,src=/home/zhangxg/work/gitrepo/github,dst=/host cr.d.xiaomi.net/cloud-ml/tensorflow-gpu:1.6.0-xm1.0.0 bash


docker run -it --privileged -p 127.0.0.1:8080:6006 --mount type=bind,src=/home/zhangxg/work/gitrepo/github,dst=/host cr.d.xiaomi.net/cloud-ml/tensorflow-gpu:1.6.0-xm1.0.0 bash

command to run in the server: 

docker run -it -v /usr/lib/x86_64-linux-gnu/libcuda.so:/usr/lib/x86_64-linux-gnu/libcuda.so -v /usr/lib/x86_64-linux-gnu/libcuda.so.1:/usr/lib/x86_64-linux-gnu/libcuda.so.1 -v /usr/lib/x86_64-linux-gnu/libcuda.so.384.111:/usr/lib/x86_64-linux-gnu/libcuda.so.384.111 --device /dev/nvidia0:/dev/nvidia0 --device /dev/nvidiactl:/dev/nvidiactl --device /dev/nvidia-modeset:/dev/nvidia-modeset --device /dev/nvidia-uvm:/dev/nvidia-uvm --device /dev/nvidia-uvm-tools:/dev/nvidia-uvm-tools cr.d.xiaomi.net/cloud-ml/tensorflow-gpu:1.6.0-xm1.0.0 bash


nvidia-docker run -it --privileged --device /dev/nvidia0:/dev/nvidia0 --device /dev/nvidia1:/dev/nvidia1 --device /dev/nvidia2:/dev/nvidia2 --device /dev/nvidia3:/dev/nvidia3 --device /dev/nvidiactl:/dev/nvidiactl --device /dev/nvidia-modeset:/dev/nvidia-modeset --device /dev/nvidia-uvm:/dev/nvidia-uvm --device /dev/nvidia-uvm-tools:/dev/nvidia-uvm-tools cr.d.xiaomi.net/cloud-ml/tensorflow-gpu:1.6.0-xm1.0.0 bash


nvidia-docker run -it -v /home/work/zhangxg:/host cr.d.xiaomi.net/zhangxiaogang/tensorflow-gpu:1.6.0 bash


nvidia-docker run -it --mount type=bind,src=/home/zhangxg/work/gitrepo/github,dst=/host cr.d.xiaomi.net/zhangxiaogang/tensorflow-gpu:1.6.0 bash

nvidia-docker run -it -v /home/work/zhangxg:/host cr.d.xiaomi.net/cloud-ml/tensorflow-gpu:1.6.0-xm1.0.0 bash

nvidia-docker run -it -v /home/work/zhangxg:/host cr.d.xiaomi.net/cloud-ml/tensorflow-gpu:1.3.0-xm1.0.0 bash

nvidia-docker run -it -v /home/work/zhangxg:/host cr.d.xiaomi.net/cloud-ml/tensorflow-gpu:1.1.0-xm1.0.0 bash


docker run -it --privileged --device /dev/nvidia0:/dev/nvidia0 --device /dev/nvidia1:/dev/nvidia1 --device /dev/nvidia2:/dev/nvidia2 --device /dev/nvidia3:/dev/nvidia3 --device /dev/nvidiactl:/dev/nvidiactl --device /dev/nvidia-modeset:/dev/nvidia-modeset --device /dev/nvidia-uvm:/dev/nvidia-uvm --device /dev/nvidia-uvm-tools:/dev/nvidia-uvm-tools cr.d.xiaomi.net/cloud-ml/tensorflow-gpu:1.6.0-xm1.0.0 bash


docker run -it --privileged -v /home/work/zhangxg:/host -v /usr/lib64/nvidia/:/usr/lib64/nvidia/ --device /dev/nvidia0:/dev/nvidia0 --device /dev/nvidia1:/dev/nvidia1 --device /dev/nvidia2:/dev/nvidia2 --device /dev/nvidia3:/dev/nvidia3 --device /dev/nvidiactl:/dev/nvidiactl --device /dev/nvidia-modeset:/dev/nvidia-modeset --device /dev/nvidia-uvm:/dev/nvidia-uvm --device /dev/nvidia-uvm-tools:/dev/nvidia-uvm-tools cr.d.xiaomi.net/cloud-ml/tensorflow-gpu:1.6.0-xm1.0.0 bash

docker run -it --privileged -v /home/work/zhangxg:/host -v /usr/lib64/nvidia/:/usr/lib64/nvidia/ --device /dev/nvidia0:/dev/nvidia0 --device /dev/nvidia1:/dev/nvidia1 --device /dev/nvidia2:/dev/nvidia2 --device /dev/nvidia3:/dev/nvidia3 --device /dev/nvidiactl:/dev/nvidiactl --device /dev/nvidia-modeset:/dev/nvidia-modeset --device /dev/nvidia-uvm:/dev/nvidia-uvm --device /dev/nvidia-uvm-tools:/dev/nvidia-uvm-tools cr.d.xiaomi.net/zhangxiaogang/tensorflow-gpu:1.6.0 bash


docker run -it --privileged -v /home/work/zhangxg:/host -v /usr/lib64/nvidia/:/usr/lib64/nvidia/ --device /dev/nvidia0:/dev/nvidia0 --device /dev/nvidia1:/dev/nvidia1 --device /dev/nvidia2:/dev/nvidia2 --device /dev/nvidia3:/dev/nvidia3 --device /dev/nvidiactl:/dev/nvidiactl --device /dev/nvidia-modeset:/dev/nvidia-modeset --device /dev/nvidia-uvm:/dev/nvidia-uvm --device /dev/nvidia-uvm-tools:/dev/nvidia-uvm-tools cr.d.xiaomi.net/cloud-ml/tensorflow-gpu:1.6.0-xm1.0.0 bash




touch test_tf_gpu.py
cat <<EOT>> test_tf_gpu.py
import tensorflow as tf
hello = tf.constant('Hello, Tensorflow!')
sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
print(sess.run(hello))
EOT
python test_tf_gpu.py



--- test fuse: 

echo "nameserver 10.237.8.8" >> /etc/resolve.conf
apt-get install inetutils-ping


export XIAOMI_ACCESS_KEY=AKTYMBAWBLTM2I5C4B
export XIAOMI_SECRET_KEY=Pu6hEuDeWU6ujJaolION8Ytapas+3o44g9q0fSNp
export XIAOMI_ACCESS_KEY_ID=AKTYMBAWBLTM2I5C4B
export XIAOMI_SECRET_ACCESS_KEY=Pu6hEuDeWU6ujJaolION8Ytapas+3o44g9q0fSNp
export XIAOMI_FDS_ENDPOINT=cnbj1-fds.api.xiaomi.net

fdsfuse test-bucket-xg /fds -o use_cache=/fdscache -o dbglevel=dbg -o curldbg

fdsfuse frameworks-samples /fds -o use_cache=/fdscache -o dbglevel=dbg -o curldbg

--- test tensorboard
tensorboard --logdir=/tmp/mnist_convnet_model/

accss in host: localhost:8080


--- test dev
nvidia-docker run -it --privileged -p 8080:6006 -p8888:8888 --mount type=bind,src=/home/zhangxg/work/gitrepo/github,dst=/host cr.d.xiaomi.net/cloud-ml/tensorflow-gpu:1.6.0-xm1.0.0 bash

nvidia-docker run -it --privileged -p 8080:6006 -p8888:8888 --mount type=bind,src=/home/zhangxg/work/gitrepo/github,dst=/host cr.d.xiaomi.net/cloud-ml/tensorflow-gpu:1.7.0-xm1.0.0 bash

jupyter notebook --allow-root


localhost:8888/?token=>????????


--- test train with fuse

mount the fuse

python mnist_fuse.py



---- push the images: 
docker login cr.d.xiaomi.net 
zhangxiaogang/a9f0cea0db92c8028898bbddea777813
docker push cr.d.xiaomi.net/cloud-ml/tensorflow-gpu:1.6.0-xm1.0.0

=== problems:

root@22e7a6c26059:/# ./nvidia-smi 
NVIDIA-SMI couldn't find libnvidia-ml.so library in your system. Please make sure that the NVIDIA Display Driver is properly installed and present in your system.
Please also try adding directory that contains libnvidia-ml.so to your system PATH.

-- 

export PATH=/usr/local/cuda-9.0/targets/x86_64-linux/lib/stubs:$PATH
/usr/local/cuda-9.0/targets/x86_64-linux/lib/stubs/libnvidia-ml.so

export PATH=/usr/local/cuda-9.0/targets/x86_64-linux/lib/stubs:/usr/local/nvidia/bin:/usr/local/cuda/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/hadoop/jdk1.6.0_37/bin:/hadoop/hadoop-2.4.0-mdh2.3.31/fuse-dfs/






problems: 

1. root@a38d6c1f4f22:/tmp# mount | grep resolv
/dev/sda6 on /etc/resolv.conf type ext4 (rw,relatime,data=ordered)


docker run --runc=nvidia

2. docker run with problems
```
root@d652cdcb5559:/host/learning_deeper/mnist_with_tensorflow# python mnist_training.py 
Traceback (most recent call last):
  File "mnist_training.py", line 7, in <module>
    import tensorflow as tf
  File "/usr/local/lib/python2.7/dist-packages/tensorflow/__init__.py", line 24, in <module>
    from tensorflow.python import *
  File "/usr/local/lib/python2.7/dist-packages/tensorflow/python/__init__.py", line 49, in <module>
    from tensorflow.python import pywrap_tensorflow
  File "/usr/local/lib/python2.7/dist-packages/tensorflow/python/pywrap_tensorflow.py", line 74, in <module>
    raise ImportError(msg)
ImportError: Traceback (most recent call last):
  File "/usr/local/lib/python2.7/dist-packages/tensorflow/python/pywrap_tensorflow.py", line 58, in <module>
    from tensorflow.python.pywrap_tensorflow_internal import *
  File "/usr/local/lib/python2.7/dist-packages/tensorflow/python/pywrap_tensorflow_internal.py", line 28, in <module>
    _pywrap_tensorflow_internal = swig_import_helper()
  File "/usr/local/lib/python2.7/dist-packages/tensorflow/python/pywrap_tensorflow_internal.py", line 24, in swig_import_helper
    _mod = imp.load_module('_pywrap_tensorflow_internal', fp, pathname, description)
ImportError: libcuda.so.1: cannot open shared object file: No such file or directory

```

ln -s /usr/local/cuda/targets/x86_64-linux/lib/stubs/libcuda.so /usr/local/nvidia/lib64/libcuda.so.1


3. docker run with problem:
```
2018-04-04 09:36:21.995336: I tensorflow/core/platform/cpu_feature_guard.cc:140] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
2018-04-04 09:36:21.995594: E tensorflow/stream_executor/cuda/cuda_driver.cc:406] failed call to cuInit: CUresult(-1)
2018-04-04 09:36:21.995624: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:158] retrieving CUDA diagnostic information for host: d652cdcb5559
2018-04-04 09:36:21.995631: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:165] hostname: d652cdcb5559
2018-04-04 09:36:21.995709: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:189] libcuda reported version is: Not found: was unable to find libcuda.so DSO loaded into this program
2018-04-04 09:36:21.995737: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:193] kernel reported version is: 384.111.0
INFO:tensorflow:Running local_init_op.

```


ln -s /usr/local/cuda/targets/x86_64-linux/lib/stubs/libcuda.so /usr/local/nvidia/lib64/libcuda.so

root@d652cdcb5559:/usr/local/nvidia/lib64# printenv | grep LD
OLDPWD=/host/learning_deeper/mnist_with_tensorflow
LD_LIBRARY_PATH=/usr/local/nvidia/lib:/usr/local/nvidia/lib64:/usr/local/lib


root@ab2de18590f7:/# printenv | grep LD
LD_LIBRARY_PATH=/usr/local/nvidia/lib:/usr/local/nvidia/lib64:/usr/local/lib

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/targets/x86_64-linux/lib/stubs/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/lib64/stubs/


THIS COMMAND ALSO WORKS: 
docker run -it --runtime=nvidia --mount type=bind,src=/home/zhangxg/work/gitrepo/github,dst=/host cr.d.xiaomi.net/cloud-ml/tensorflow-gpu:1.6.0-xm1.0.0 bash

or by changing the docker `sudo vim /etc/docker/daemon.json`

```
{
   "default-runtime": "nvidia",
   "runtimes": {
       "nvidia": {
           "path": "/usr/bin/nvidia-container-runtime",
           "runtimeArgs": []
       }
   }
}
```

```
systemctl restart docker
```

start docker without using the `--runtime`.


docker network: 

docker dockerfile making: 

confirm with k8s team on the changing of the docker runtime.

====
```
the images is growing in size: 
cr.d.xiaomi.net/cloud-ml/tensorflow-gpu                       1.6.0-xm1.0.0                ce5c285eb243        14 minutes ago      5.71 GB
cr.d.xiaomi.net/zhangbo11/tensorflow-gpu                      <none>                       b71281c73217        2 months ago        4.85 GB
cr.d.xiaomi.net/zhangbo11/tensorflow-gpu                      1.3.0-xm1.0.0-keras          cc6b988f52bb        2 months ago        4.61 GB
cr.d.xiaomi.net/cloud-ml/model-tensorflow-gpu                 1.3.0-xm1.0.0                61165af5200b        2 months ago        4.86 GB
cr.d.xiaomi.net/cloud-ml/tensorflow-gpu                       1.3.0-xm1.0.0                e2080171ba0c        3 months ago        4.18 GB
cr.d.xiaomi.net/cloud-ml/tensorflow-gpu                       <none>                       243eeb82d19b        3 months ago        4.18 GB
cr.d.xiaomi.net/cloud-ml/tensorflow-gpu                       1.0.0-xm1.0.0                d931d91c6363        3 months ago        3.32 GB
cr.d.xiaomi.net/cloud-ml/tensorflow-gpu                       1.3.0-xm1.0.0-yulianfei      37701895348e        3 months ago        4.8 GB
cr.d.xiaomi.net/wangxingyou/hacker-gpu                        tensorflow-1.4               e051f0b8382c        4 months ago        3.46 GB
cr.d.xiaomi.net/cloud-ml/tensorflow-gpu                       1.1.0-xm1.0.0                b6886f17a9e3        4 months ago        3.25 GB
cr.d.xiaomi.net/cloud-ml/tensorflow-gpu                       <none>                       279ecf8b9f54        4 months ago        3.32 GB
docker.d.xiaomi.net/cloud-ml/tensorflow-gpu                   1.3.0-xm1.0.0                29697abcf3d8        5 months ago        3.49 GB
docker.d.xiaomi.net/cloud-ml/tensorflow-gpu                   1.0.0-xm1.0.0                1632357fb2f5        5 months ago        3.31 GB
docker.d.xiaomi.net/cloud-ml/dev-tensorflow-gpu               1.3.0-xm1.0.0-c3prc-hadoop   36ce07d8a701        5 months ago        4.67 GB
cr.d.xiaomi.net/cloud-ml/model-tensorflow-gpu                 <none>                       25f98e1cb239        6 months ago        4.51 GB
docker.d.xiaomi.net/cloud-ml/train-tensorflow-cpu             1.0.0-xm1.0.0-c3prc-hadoop   3a6b555280b0        6 months ago        2.16 GB
docker.d.xiaomi.net/cloud-ml/train-tensorflow-cpu             1.1.0-xm1.0.0                f654bb7b3355        6 months ago        1.78 GB
docker.d.xiaomi.net/cloud-ml/dev-tensorflow-gpu               1.0.0-xm1.0.0                5de72c5429a6        6 months ago        3.36 GB
docker.d.xiaomi.net/cloud-ml/train-tensorflow-gpu             1.0.0-xm1.0.0                6466ebd40c1e        6 months ago        3.33 GB
docker.d.xiaomi.net/cloud-ml/train-tensorflow-cpu             1.0.0-xm1.0.0                a591cb43ed9b        6 months ago        1.52 GB
docker.d.xiaomi.net/cloud-ml/base-tensorflow-cpu              <none>                       0ea693b6e470        6 months ago        1.52 GB
docker.d.xiaomi.net/dl-framework/tensorflow_gpu               v1.3.0_xm1.0                 e6490366389e        6 months ago        4.8 GB
cr.d.xiaomi.net/cloud-ml/train-tensorflow-cpu                 1.0.0-xm1.0.0                3a2610466a19        7 months ago        1.45 GB
docker.d.xiaomi.net/cloud-ml/base-tensorflow-cpu              1.0.0-xm1.0.0                ef4a0a4e0a2c        7 months ago        1.45 GB
```

we need to clean the unused images, the house keeping;

get the source code in the remote server: 

git clone https://github.com/zhangxg/learning_deeper.git
or 
wget https://github.com/zhangxg/learning_deeper/archive/master.zip


in centos:
the kubelet configuration:
/usr/lib/systemd/system


question is: how kubernetes/kubelet starts the container; would like to know the commnad sent to docker? 

how kubernetes is gpu aware? 


===== mxnet: 



nvidia-docker run -it -v /home/work/zhangxg:/host cr.d.xiaomi.net/cloud-ml/mxnet:1.1.0-xm1.0.0 bash

nvidia-docker run -it --mount type=bind,src=/home/zhangxg/work/gitrepo/github,dst=/host cr.d.xiaomi.net/cloud-ml/mxnet:1.1.0-xm1.0.0 bash


docker run -it --privileged -v /home/work/zhangxg:/host -v /usr/lib64/nvidia/:/usr/lib64/nvidia/ --device /dev/nvidia0:/dev/nvidia0 --device /dev/nvidiactl:/dev/nvidiactl --device /dev/nvidia-modeset:/dev/nvidia-modeset --device /dev/nvidia-uvm:/dev/nvidia-uvm --device /dev/nvidia-uvm-tools:/dev/nvidia-uvm-tools cr.d.xiaomi.net/cloud-ml/mxnet:1.1.0-xm1.0.0 bash


touch test_mxnet_gpu.py
cat <<EOT>> test_mxnet_gpu.py
import mxnet as mx
a = mx.nd.ones((2, 3), mx.gpu())
b = a * 2 + 1
b.asnumpy()
EOT
python test_mxnet_gpu.py


cd cd incubator-mxnet/example/image-classification/
python train_mnist.py


docker push cr.d.xiaomi.net/cloud-ml/mxnet:1.1.0-xm1.0.0
docker tag cr.d.xiaomi.net/cloud-ml/mxnet:1.1.0-xm1.0.0 cnbj6-repo.cloud.mi.com/cloud-ml/mxnet:1.1.0-xm1.0.0
docker push cnbj6-repo.cloud.mi.com/cloud-ml/mxnet:1.1.0-xm1.0.0



==
for mxnet, `-v /usr/lib64/nvidia/:/usr/lib64/nvidia/` has no effect, it always works


docker run -it --privileged -v /home/zhangxg/work:/host --device /dev/nvidia0:/dev/nvidia0 --device /dev/nvidiactl:/dev/nvidiactl --device /dev/nvidia-modeset:/dev/nvidia-modeset --device /dev/nvidia-uvm:/dev/nvidia-uvm --device /dev/nvidia-uvm-tools:/dev/nvidia-uvm-tools cr.d.xiaomi.net/cloud-ml/mxnet:1.1.0-xm1.0.0 bash


docker run -it --privileged -v /home/zhangxg/work:/host -v /usr/lib64/nvidia/:/usr/lib64/nvidia/ --device /dev/nvidia0:/dev/nvidia0 --device /dev/nvidiactl:/dev/nvidiactl --device /dev/nvidia-modeset:/dev/nvidia-modeset --device /dev/nvidia-uvm:/dev/nvidia-uvm --device /dev/nvidia-uvm-tools:/dev/nvidia-uvm-tools cr.d.xiaomi.net/cloud-ml/mxnet:1.1.0-xm1.0.0 bash


-- for tensorflow, `-v /usr/lib64/nvidia/:/usr/lib64/nvidia/` has no effect, it always doesn't work

docker run -it --privileged -v /home/work/zhangxg/:/host -v /usr/lib64/nvidia/:/usr/lib64/nvidia/ --device /dev/nvidia0:/dev/nvidia0 --device /dev/nvidiactl:/dev/nvidiactl --device /dev/nvidia-modeset:/dev/nvidia-modeset --device /dev/nvidia-uvm:/dev/nvidia-uvm --device /dev/nvidia-uvm-tools:/dev/nvidia-uvm-tools cr.d.xiaomi.net/cloud-ml/tensorflow-gpu:1.7.0-xm1.0.0 bash


nvidia-docker run -it --privileged --mount type=bind,src=/home/zhangxg/work/gitrepo/github,dst=/host cr.d.xiaomi.net/cloud-ml/tensorflow-gpu:1.6.0-xm1.0.0 bash


--- shengtaiyun:

2018-04-25 07:35:49.100963: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:898] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUM
A node, so returning NUMA node zero
2018-04-25 07:35:49.101801: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1344] Found device 3 with properties: 
name: Tesla P40 major: 6 minor: 1 memoryClockRate(GHz): 1.531
pciBusID: 0000:00:0a.0
totalMemory: 22.38GiB freeMemory: 22.22GiB
2018-04-25 07:35:49.102063: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1423] Adding visible gpu devices: 0, 1, 2, 3
2018-04-25 07:35:49.102242: E tensorflow/core/common_runtime/direct_session.cc:167] Internal: cudaGetDevice() failed. Status: CUDA driver version is insufficient for CUDA runtime versi
on

-- shengtaiyun driver version:
[root@vm10-8-0-16 zhangxg]# nvidia-smi 
Wed Apr 25 15:45:18 2018       
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 375.26                 Driver Version: 375.26                    |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  Tesla P40           On   | 0000:00:07.0     Off |                    0 |
| N/A   19C    P8    10W / 250W |      0MiB / 22912MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
|   1  Tesla P40           On   | 0000:00:08.0     Off |                    0 |
| N/A   20C    P8    10W / 250W |      0MiB / 22912MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
|   2  Tesla P40           On   | 0000:00:09.0     Off |                    0 |
| N/A   20C    P8    11W / 250W |      0MiB / 22912MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
|   3  Tesla P40           On   | 0000:00:0A.0     Off |                    0 |
| N/A   20C    P8    11W / 250W |      0MiB / 22912MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                       GPU Memory |
|  GPU       PID  Type  Process name                               Usage      |
|=============================================================================|
|  No running processes found                                                 |
+-----------------------------------------------------------------------------+

upgrade: 

[root@vm10-8-0-16 ~]# nvidia-smi 
Wed Apr 25 18:28:25 2018       
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 390.30                 Driver Version: 390.30                    |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  Tesla P40           On   | 00000000:00:07.0 Off |                    0 |
| N/A   20C    P8    10W / 250W |      0MiB / 22919MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
|   1  Tesla P40           On   | 00000000:00:08.0 Off |                    0 |
| N/A   27C    P0    55W / 250W |      0MiB / 22919MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
|   2  Tesla P40           On   | 00000000:00:09.0 Off |                    0 |
| N/A   21C    P8    11W / 250W |      0MiB / 22919MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
|   3  Tesla P40           On   | 00000000:00:0A.0 Off |                    0 |
| N/A   21C    P8    11W / 250W |      0MiB / 22919MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                       GPU Memory |
|  GPU       PID   Type   Process name                             Usage      |
|=============================================================================|
|  No running processes found                                                 |
+-----------------------------------------------------------------------------+


-- rongheyun driver version:
[work@tj-hadoop-dl87 ~]$ nvidia-smi 
Wed Apr 25 15:45:04 2018       
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 390.12                 Driver Version: 390.12                    |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  Tesla P40           On   | 00000000:02:00.0 Off |                    0 |
| N/A   35C    P0    57W / 250W |  21857MiB / 22919MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
|   1  Tesla P40           On   | 00000000:03:00.0 Off |                    0 |
| N/A   28C    P0    49W / 250W |  21855MiB / 22919MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
|   2  Tesla P40           On   | 00000000:83:00.0 Off |                    0 |
| N/A   31C    P0    50W / 250W |  21857MiB / 22919MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
|   3  Tesla P40           On   | 00000000:84:00.0 Off |                    0 |
| N/A   19C    P8    10W / 250W |      0MiB / 22919MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                       GPU Memory |
|  GPU       PID   Type   Process name                             Usage      |
|=============================================================================|
|    0      6507      C   /tensorflow_model_server                   21845MiB |
|    1      6508      C   /tensorflow_model_server                   21845MiB |
|    2     51245      C   /tensorflow_model_server                   21845MiB |
+-----------------------------------------------------------------------------+




### test modelserver

docker run -it --privileged -v /home/zhangxg/work/gitrepo/github:/host -v /usr/lib64/nvidia/:/usr/lib64/nvidia/ --device /dev/nvidia0:/dev/nvidia0 --device /dev/nvidiactl:/dev/nvidiactl --device /dev/nvidia-modeset:/dev/nvidia-modeset --device /dev/nvidia-uvm:/dev/nvidia-uvm --device /dev/nvidia-uvm-tools:/dev/nvidia-uvm-tools cr.d.xiaomi.net/zhangxiaogang/tensorflow-gpu:1.0.0-xm1.0.0-modelserver bash


### nvidia-smi data collect
nvidia-smi --query-gpu=timestamp,name,pci.bus_id,utilization.gpu,utilization.memory,memory.total,memory.free,memory.used,driver_version,pstate,pcie.link.gen.max,pcie.link.gen.current,temperature.gpu, --format=csv -l 5 -f "nvidia-smi_output.txt"


[see here](https://nvidia.custhelp.com/app/answers/detail/a_id/3751/~/useful-nvidia-smi-queries)



