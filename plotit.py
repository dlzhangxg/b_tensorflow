from plotly.offline import init_notebook_mode, iplot, offline
from plotly import graph_objs as go
init_notebook_mode()

class LineChart(object):

  def __init__(self, title, x_axis_title, y_axis_title, second_y_axis_title=None):
    self.title = title
    self.x_axis_title = x_axis_title
    self.y_axis_title = y_axis_title
    self.second_y_axis_title = second_y_axis_title

  def get_default_layout(self):
    layout = {
      "title": self.title, 
      "legend": {
        "x": 0.2,
        "y": 1,
        "orientation":"h"
      },
      "xaxis": {"title": self.x_axis_title}, 
      "yaxis": {"title": self.y_axis_title}
    }

    if self.second_y_axis_title:
      layout["yaxis2"] = {
        "title": self.second_y_axis_title,
        "overlaying": "y", 
        "side": "right", 
      }
    return layout

  def generate_graph_data(self, x, y, name, mode="lines+markers", second_y_axis=False):
    return go.Scatter(
      x = x,
      y = y,
      mode = mode,
      name = name, 
      yaxis = "y2" if second_y_axis else "y1",
    )

  def draw(self, data):
    fig = go.Figure(data=data, layout=self.get_default_layout())
    offline.iplot(fig)