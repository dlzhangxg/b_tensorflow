FROM ubuntu:16.04

#---- cuda and cudnn -------
# install driver
RUN apt-get update && apt-get install -y --no-install-recommends \
       nvidia-375 

ENV DEBIAN_FRONTEND noninteractive

## install cuda 9.0
COPY  cuda-repo-ubuntu1604-8-0-local-ga2_8.0.61-1_amd64.deb .
RUN   dpkg -i cuda-repo-ubuntu1604-8-0-local-ga2_8.0.61-1_amd64.deb && \
      apt-get update && \
      apt-get install -y --no-install-recommends \
      cuda 

# install cudnn6
COPY  cudnn-8.0-linux-x64-v6.0.tgz . 
RUN tar -xzvf /cudnn-8.0-linux-x64-v6.0.tgz && \
    cp cuda/lib64/* /usr/local/cuda/lib64/ && \
    cp cuda/include/cudnn.h /usr/local/cuda/include/


ENV  PATH=/usr/local/cuda/bin:$PATH \
     LD_LIBRARY_PATH=/usr/local/cuda/lib64:$LD_LIBRARY_PATH

RUN rm -rf cuda-repo-ubuntu1604-8-0-local-ga2_8.0.61-1_amd64.deb \
    cudnn-8.0-linux-x64-v6.0.tgz \
    cuda


#---- packages -------
RUN apt-get update && apt-get install -y --no-install-recommends \ 
    curl  \
    wget \
    git

# ubuntu 16 defaults to python3
RUN ln -s /usr/bin/python3 /usr/bin/python

# install pip
RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && \
    python get-pip.py && \
    rm get-pip.py

RUN apt-get update && apt-get install -y --no-install-recommends \ 
    vim


RUN pip install tensorflow-gpu -i https://pypi.douban.com/simple
