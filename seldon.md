

# run the seldon model server

## wrap the model

```
cd /home/zhangxg/work/gitrepo/github/example-seldon
```


train the model:

```/home/zhangxg/work/gitrepo/github/example-seldon/models/sk_mnist/train
python create_model.py 
```

the model is stored into:
```sk_mnist/model_ouput/sk.pkl
```


goes to `runtime` folder

```
cd /home/zhangxg/work/gitrepo/github/example-seldon/models/sk_mnist/runtime
cp ../model_ouput/sk.pkl .
```

now the folder looks like this:

```➜  runtime git:(master) ✗ tree -L 1                 
.
├── contract.json
├── Makefile
├── requirements.txt
├── SkMnist.py
├── sk.pkl
└── wrap.sh
```

`SkMnist.py` the client code
`sk.pkl` the copied model
`requirements.txt` is the dependency


### make the distributable

```
docker run -v /home/zhangxg/work/gitrepo/github/example-seldon/models/sk_mnist/runtime:/my_model seldonio/core-python-wrapper:0.7 /my_model SkMnist 0.2 cr.d.xiaomi.net/zhangxiaogang
```

this generate the build template, now the folder looks like this: 

```➜  runtime git:(master) ✗ tree -L 1
.
├── build
├── contract.json
├── Makefile
├── requirements.txt
├── SkMnist.py
├── sk.pkl
└── wrap.sh
```

`build` folder is created and which contains:

```
➜  build git:(master) ✗ {pwd; tree -L 1}

/home/zhangxg/work/gitrepo/github/example-seldon/models/sk_mnist/runtime/build
.
├── build_image.sh
├── contract.json
├── Dockerfile
├── Makefile
├── microservice.py
├── model_microservice.py
├── persistence.py
├── proto
├── push_image.sh
├── README.md
├── requirements.txt
├── seldon_requirements.txt
├── SkMnist.py
├── sk.pkl
└── wrap.sh

```

review the `Dockerfile`

```
FROM python:2

LABEL "docker_repo"="cr.d.xiaomi.net/zhangxiaogang"
LABEL "docker_image_name"="skmnist"
LABEL "docker_image_version"="0.2"
LABEL "service_type"="MODEL"
LABEL "base_image"="python:2"
LABEL "model_name"="SkMnist"
LABEL "persistence"="0"
LABEL "api_type"="REST"

RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential

COPY /requirements.txt /tmp/
COPY /seldon_requirements.txt /tmp/
RUN cd /tmp && \
    pip install --no-cache-dir -r seldon_requirements.txt && \
    pip install --no-cache-dir -r requirements.txt

RUN mkdir microservice
COPY ./ /microservice/
WORKDIR /microservice

EXPOSE 5000
CMD ["python","-u","microservice.py","SkMnist","REST","--service-type","MODEL","--persistence","0"]
```


change `FROM python:2` to `FROM python:3.6`, [the code depends on 3.6] and build. 

```
sudo vim Dockerfile
```

Build and push the Docker image

```
cd /home/zhangxg/work/gitrepo/github/example-seldon/models/sk_mnist/runtime/build
./build_image.sh
```

the built docker images:
```
➜  build git:(master) ✗ docker images
REPOSITORY                                            TAG                            IMAGE ID            CREATED             SIZE
cr.d.xiaomi.net/zhangxiaogang/skmnist                 0.2                            1a10a814e4b7        13 minutes ago      1.31GB
```


push images:

```
./push_image.sh
```


## deploy the model

goes to the kubernetes controller node, 

```
tj1-cloudml-i1-stress01.kscn
```

the deployment descriptor

```
{
    "apiVersion": "machinelearning.seldon.io/v1alpha1",
    "kind": "SeldonDeployment",
    "metadata": {
        "labels": {
            "app": "seldon"
        },
        "name": "skmnist"
    },
    "spec": {
        "annotations": {
            "project_name": "FX Market Prediction-1",
            "deployment_version": "v0.1"
        },
        "name": "skmnist",
        "oauth_key": "oauth-key-a",
        "oauth_secret": "oauth-secret-a",
        "predictors": [
            {
                "componentSpec": {
                    "spec": {
                        "containers": [
                            {
                                "image": "cr.d.xiaomi.net/zhangxiaogang/skmnist:0.2",
                                "imagePullPolicy": "IfNotPresent",
                                "name": "skmnist",
                                "resources": {
                                    "requests": {
                                        "memory": "200Mi"
                                    }
                                }
                            }
                        ],
                        "terminationGracePeriodSeconds": 20
                    }
                },
                "graph": {
                    "children": [],
                    "name": "skmnist",
                    "endpoint": {
                        "type" : "REST"
                    },
                    "type": "MODEL"
                },
                "name": "fx-market-predictor",
                "replicas": 1,
                "annotations": {
                    "predictor_version" : "v0.1"
                }
            }
        ]
    }
}
```


delete the modesl:
```
k delete deploy skmnist-fx-market-predictor -n seldon
```



```
[work@tj1-cloudml-i1-stress01 zhangxg]$ kubectl apply -f model.json -n seldon
seldondeployment "skmnist2" configured
```


```
[work@tj1-cloudml-i1-stress01 zhangxg]$ k get po -n seldon
NAME                                              READY     STATUS    RESTARTS   AGE
graph-fx-market-predictor-2108730359-jj7dr        2/2       Running   0          4d
graph-grpc-fx-market-predictor-3031937618-nzbwr   2/2       Running   0          1d
redis-4232925728-vxl2h                            1/1       Running   0          6d
seldon-apiserver-2985163275-r18nm                 1/1       Running   0          6d
seldon-cluster-manager-1769291715-nfzfc           1/1       Running   0          6d
skmnist-fx-market-predictor-3822629188-fcbl1      2/2       Running   0          51s
```


```
[work@tj1-cloudml-i1-stress01 zhangxg]$ k get svc -n seldon
NAME               CLUSTER-IP       EXTERNAL-IP   PORT(S)                         AGE
graph              10.254.20.51     <none>        8000/TCP,5001/TCP               6d
graph-grpc         10.254.9.121     <none>        8000/TCP,5001/TCP               1d
redis              10.254.195.211   <none>        6379/TCP                        6d
seldon-apiserver   10.254.9.34      <nodes>       8080:30126/TCP,5000:31783/TCP   6d
skmnist            10.254.155.173   <none>        8000/TCP,5001/TCP               5d
```

seldon-apiserver port [30126] 

## run clients

`client.py`

```
import requests
from requests.auth import HTTPBasicAuth
from proto import prediction_pb2
from proto import prediction_pb2_grpc
import grpc
from sklearn.ensemble import RandomForestClassifier
from sklearn import datasets, metrics
from sklearn.utils import shuffle
from sklearn.datasets import fetch_mldata
from sklearn.externals import joblib


try:
  from commands import getoutput  # python 2
except ImportError:
  from subprocess import getoutput  # python 3

NAMESPACE = 'seldon'
#MINIKUBE_IP = getoutput(' minikube-linux-amd64 ip')
MINIKUBE_IP = "10.38.10.152"
# MINIKUBE_HTTP_PORT = getoutput(
#  "kubectl get svc -n " + NAMESPACE + " -l app=seldon-apiserver-container-app -o jsonpath='{.items[0].spec.ports[0].nodePort}'")
MINIKUBE_HTTP_PORT = "80"
#MINIKUBE_GRPC_PORT = getoutput(
#  "kubectl get svc -n " + NAMESPACE + " -l app=seldon-apiserver-container-app -o jsonpath='{.items[0].spec.ports[1].nodePort}'")

print(MINIKUBE_IP)
print(MINIKUBE_HTTP_PORT)

def get_token():
  payload = {'grant_type': 'client_credentials'}
  response = requests.post(
    "http://" + MINIKUBE_IP + ":" + MINIKUBE_HTTP_PORT + "/http/oauth/token",
    auth=HTTPBasicAuth('oauth-key-a', 'oauth-secret-a'),
    data=payload)
  print(response.text)
  print(response.url)
  token = response.json()["access_token"]
  return token

def rest_request():
  token = get_token()
  headers = {'Authorization': 'Bearer ' + token}

  mnist = datasets.load_digits()
  n_samples = len(mnist.images)


  data = mnist.images.reshape((n_samples, -1))
  targets = mnist.target

  data,targets = shuffle(data,targets)


  payload = {"data":{"names":["a"],"ndarray":data.tolist()}}
  response = requests.post(
    "http://" + MINIKUBE_IP + ":" + MINIKUBE_HTTP_PORT + "/http/api/v0.1/predictions",
    headers=headers,
    json=payload)
  print(response.text)

def grpc_request():
  token = get_token()
  datadef = prediction_pb2.DefaultData(
    x=1,
    y=1,
  )
  request = prediction_pb2.SeldonMessage(data=datadef)
  channel = grpc.insecure_channel(MINIKUBE_IP + ":" + MINIKUBE_GRPC_PORT)
  stub = prediction_pb2_grpc.SeldonStub(channel)
  metadata = [('oauth_token', token)]
  response = stub.Predict(request=request, metadata=metadata)
  print(response)

rest_request()

```

`python client.py`


## delete:
```
k delete deploy skmnist-fx-market-predictor -n seldon
k delete svc skmnist -n seldon
```


## integration with cloudml

### server start up

```
== not working === 
k get po -n cloudml | tr -s " " | cut -d " " -f1 | xargs -n1 k exec -it -n cloudml $f bash
```

```
## enters the container
k exec -it -n cloudml cloudml-3472954160-xcd0b bash


cd /home/cloud-ml/restful_server
./kill_nohup

cd /home/
rsync -chavzP --stats --exclude "db.sqlite3" zhangxg@10.231.56.250:/home/zhangxg/work/gitrepo/cloudml_repo/cloud-ml .

```

cm models create -n skmnist -v 1 -fe cnbj1-fds.api.xiaomi.net -fb test-bucket-xg -a '{"seldon": "true", "tarball_path": "seldon_sklearn/sk_mnist.tar.gz", "run_class": "SkMnist"}' -u "what_ever" -d "cr.d.xiaomi.net/baiyun/seldon-server-base:0.9"


## uisng fds model_path [OK]
cm models create -n skmnist -v 1 -fb test-bucket-xg -a '{"seldon": "true","run_class": "SkMnist"}' -u "fds://seldon_sklearn/sk_mnist.tar.gz" -d "cr.d.xiaomi.net/baiyun/seldon-server-base:0.9"

## uisng http as model_path [OK]
cm models create -n skmnist -v 1 -a '{"seldon": "true","run_class": "SkMnist"}' -u "http://cnbj1-fds.api.xiaomi.net/test-bucket-xg/seldon_sklearn/sk_mnist.tar.gz" -d "cr.d.xiaomi.net/baiyun/seldon-server-base:0.9"

## without the image[NOT OK, apiserver set the default value]
cm models create -n skmnist -v 1 -a '{"seldon": "true","run_class": "SkMnist"}' -u "http://cnbj1-fds.api.xiaomi.net/test-bucket-xg/seldon_sklearn/sk_mnist.tar.gz"


## with gpu
cm models create -n translator -v 1 -fb test-bucket-xg -a '{"seldon": "true","run_class": "Translator"}' -u "fds://minmt_translator.tar.gz" -d "cr.d.xiaomi.net/cloud-ml/seldon-server-gpu:0.1" -nsk xiaomi/gpu-card -nsv m40 -c 8 -M 8G -g 1



**restart-server**

```
#! /bin/bash
cd /home/cloud-ml/restful_server
./kill_nohup
cd /home/
rsync -chavzP --stats zhangxg@10.231.56.250:/home/zhangxg/work/gitrepo/cloudml_repo/cloud-ml .

cd /home/cloud-ml/restful_server
echo "staring..."
./nohup_run
#python manage.py runserver 0.0.0.0:8000
```

scp zhangxg@10.231.56.250:/home/zhangxg/work/gitrepo/cloudml_repo/cloud-ml/restful_server/db.sqlite3 /home/cloud-ml/restful_server

1. create `k delete deploy skmnist-fx-market-predictor -n seldon; k apply -f model.json  -n seldon;`

```
### delete all seldon deployment

k get seldondeployment -n seldon | grep skmnist| tr -s " " | cut -d " " -f1 | xargs -n1 k delete seldondeployment $f -n seldon

k get deployment -n seldon | grep skmnist | tr -s " " | cut -d " " -f1 | xargs -n1 k delete deploy $f -n seldon

k get svc -n seldon | grep skmnist | tr -s " " | cut -d " " -f1 | xargs -n1 k delete svc -n seldon

```


1. 


```
sklearn
numpy
scipy
requests
protobuf
grpcio


```


## test gpu

### start the gpu container

docker run -it --privileged --device /dev/nvidia0:/dev/nvidia0 --device /dev/nvidia1:/dev/nvidia1 --device /dev/nvidia2:/dev/nvidia2 --device /dev/nvidia3:/dev/nvidia3 --device /dev/nvidiactl:/dev/nvidiactl --device /dev/nvidia-modeset:/dev/nvidia-modeset --device /dev/nvidia-uvm:/dev/nvidia-uvm --device /dev/nvidia-uvm-tools:/dev/nvidia-uvm-tools -v /home/work/zhangxg:/host cr.d.xiaomi.net/cloud-ml/seldon-server-gpu:0.1  /bin/bash


## install tensorflow-gpu
pip install tensorflow-gpu -i https://pypi.douban.com/simple

## test code
touch test_tf_gpu.py
cat <<EOT>> test_tf_gpu.py
import tensorflow as tf
hello = tf.constant('Hello, Tensorflow!')
sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
print(sess.run(hello))
EOT
python test_tf_gpu.py



scp -r zhangxg@10.231.56.250:/home/zhangxg/work/tmp/seldon_minmt/small_sized /microservice/

