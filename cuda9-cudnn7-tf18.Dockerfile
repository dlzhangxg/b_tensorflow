
# install cuda9/cudnn7/tensorflow1.8 (latest)

FROM ubuntu:16.04

ENV DEBIAN_FRONTEND noninteractive

# ubuntu:16.04 comes with no python, install python
RUN apt-get update && apt-get install -y software-properties-common
RUN add-apt-repository ppa:jonathonf/python-3.6
RUN apt-get update && apt-get install -y build-essential python3.6 python3.6-dev python3-distutils

# install frenquently used packages
RUN apt-get update && apt-get install -y --no-install-recommends \ 
    curl  \
    wget \
    git \
    vim

# ubuntu 16 defaults to python3
RUN ln -fs /usr/bin/python3.6 /usr/bin/python

# install pip
RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && \
    python get-pip.py && \
    rm get-pip.py

#---- cuda and cudnn -------
# install driver
RUN apt-get update && apt-get install -y --no-install-recommends \
       nvidia-375 

## install cuda 9.0
COPY  cuda-repo-ubuntu1604-9-0-local_9.0.176-1_amd64.deb .
RUN   dpkg -i cuda-repo-ubuntu1604-9-0-local_9.0.176-1_amd64.deb && \
      apt-get update && \
      apt-get install -y --allow-unauthenticated cuda

ENV  PATH=/usr/local/cuda/bin:$PATH \
     LD_LIBRARY_PATH=/usr/local/cuda/lib64:$LD_LIBRARY_PATH

RUN rm -rf cuda-repo-ubuntu1604-8-0-local-ga2_8.0.61-1_amd64.deb

# install cudnn6
COPY  cudnn-9.0-linux-x64-v7.tgz . 
RUN tar -xzvf /cudnn-9.0-linux-x64-v7.tgz && \
    cp cuda/lib64/* /usr/local/cuda/lib64/ && \
    cp cuda/include/cudnn.h /usr/local/cuda/include/ && \
    rm -r cudnn-9.0-linux-x64-v7.tgz cuda

RUN pip install tensorflow-gpu -i https://pypi.douban.com/simple
